﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CreateFeedDatabase
{ 
    [MenuItem("Assets/Create/Feed Database")]
    public static void CreateFeedDBAsset()
    {
        FeedDB feedDBAsset = ScriptableObject.CreateInstance<FeedDB>();
        AssetDatabase.CreateAsset(feedDBAsset, "Assets/Database/FeedDB.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();
        Selection.activeObject = feedDBAsset;

    }
}
