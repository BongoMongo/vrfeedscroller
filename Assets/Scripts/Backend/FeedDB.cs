﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;

public class FeedDB : ScriptableObject {

    public string PathToPhotospheres;
    public string PathToThumbnails;



    public ProtoViorama[] Vioramas;

    /// <summary>
    /// Returns all Vioramas of a certain category
    /// </summary>
    /// <param name="in_CategoryName">The Name of the category</param>
    /// <returns>A List of vioramas</returns>
    public List<ProtoViorama> GetVioramas(string in_CategoryName)
    {
        List<ProtoViorama> l_Vioramas = new List<ProtoViorama>();

        // since category descriptor is passed here, retrieve the corresponding enum value
        ProtoViorama.Categories cat = (ProtoViorama.Categories)EnumUtils.enumValueOf(in_CategoryName, typeof(ProtoViorama.Categories));

        for(int i=0; i< Vioramas.Length; ++i)
        {
            if(Vioramas[i].Category == cat)
            {
                l_Vioramas.Add(Vioramas[i]);
            }
        }
        return l_Vioramas;
    }

    /// <summary>
    /// Returns a list of categories. Note that not the enum itself is used but the enum descriptor
    /// </summary>
    /// <returns>All categories as a list</returns>
    public List<string> GetCategories()
    {
        List<string> l_Categories = new List<string>();

        foreach (ProtoViorama.Categories category in Enum.GetValues(typeof(ProtoViorama.Categories)))
        {
            l_Categories.Add(EnumUtils.stringValueOf(category));
        }

        return l_Categories;
    }
}
