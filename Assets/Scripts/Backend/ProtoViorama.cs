﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;

[System.Serializable]
public class ProtoViorama
{
    public enum Categories
    {
        [Description("Art")]
        Art,
        [Description("Desert")]
        Desert,
        [Description("Videos")]
        Videos,
        [Description("Landscapes")]
        Landscapes,
        [Description("Las Vegas")]
        LasVegas,
        [Description("Theatre")]
        Theatre,
        [Description("Fusion Festival")]
        Fusion,
        [Description("Activism")]
        Activism,
        [Description("Parties")]
        Parties,
        [Description("Today")]
        Today
    }
    [SerializeField]
    public string Title;
    public Categories Category;
    [SerializeField]
    public string PreviewURL;
    [SerializeField]
    public string ImageURL;
    [SerializeField]
    public string Author;
    [SerializeField]
    public string FileName;


}
