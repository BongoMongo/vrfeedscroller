﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Viorama
{
    /**
		Manages connection to database, manages, loads and unloads viorama spheres
	*/
    public class FeedManager : Singleton<FeedManager>
    {
        #region MEMBER VARIABLES

        // flag to switch between real and SciptableObject based database
        public bool m_IsLocalDatabase = true;

        // ScriptableObject based database
        private FeedDB m_Database;

        // dictionary that stores categories and corresponding vioramas
        private Dictionary<string, List<ProtoViorama>> m_Vioramas = new Dictionary<string, List<ProtoViorama>>();

        #endregion

        #region UNITY FUNCTIONS

        void Awake()
        {
            if (m_IsLocalDatabase == true)
            {
                Debug.Log("Establish connection to local database");
                Object feedDBObject = Resources.Load("FeedDB", typeof(FeedDB));
                FeedDB feedDB = (FeedDB)feedDBObject;

                // retrieve all categories
                List<string> l_Categories = feedDB.GetCategories();

                // get vioramas for each category
                foreach (string category in l_Categories)
                {
                    List<ProtoViorama> l_Vioramas = feedDB.GetVioramas(category);
                    // create a new category and assign vioramas
                    m_Vioramas[category] = l_Vioramas;
                }
            }
        }


        void Start()
        {

        }

        void Update()
        {

        }

        #endregion

        #region FUNCTIONS
        public List<ProtoViorama> FetchFeed(string in_Category, int in_Offset, int in_Limit)
        {
            List<ProtoViorama> l_Vioramas = new List<ProtoViorama>();

            if (m_Vioramas.ContainsKey(in_Category))
            {
                Debug.Log("Fetching " + in_Limit + " vioramas from database");
                try
                {
                    if(in_Offset < m_Vioramas[in_Category].Count)
                    {
                        // if limit exceeds amount of elements only retrieve elements from offset to end
                        if(in_Offset + in_Limit > m_Vioramas[in_Category].Count)
                        {
                            l_Vioramas = m_Vioramas[in_Category].GetRange(in_Offset, m_Vioramas[in_Category].Count - in_Offset);
                        }
                        else
                        {
                            l_Vioramas = m_Vioramas[in_Category].GetRange(in_Offset, in_Limit);
                        }
                    }
                }
                catch(System.Exception e)
                {
                    Debug.LogError(e);
                }
            }
            else
            {
                Debug.LogError("Error while requesting feed database. No such category: \"" + in_Category + "\"");
            }

            return l_Vioramas;
        }

        public string GetFirstCategory()
        {
            return EnumUtils.stringValueOf(ProtoViorama.Categories.Today);
        }

        public int GetFeedCount(string in_Category)
        {
            int l_Count = 0;

            if (m_Vioramas.ContainsKey(in_Category))
                l_Count = m_Vioramas[in_Category].Count;

            return l_Count;

        }

        #endregion

        #region EVENT HANDLER
        #endregion

        #region PROPERTIES
        #endregion

    }
}