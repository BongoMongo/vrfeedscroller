﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Viorama
{
    /**
		Sets up the feed scrolling, feed position and manages feed behavior based on user input
	*/
    public class FeedScrollController : Singleton<FeedScrollController>
    {
        private enum SpawnMode { Left, Right};

        [SerializeField]
        private Transform m_FeedTilePrefab;

        public int m_InitialPoolSize = 10;
        public int m_BatchSize = 5;

        public float m_SpawnNewFeedsAngle = 30f;

        [SerializeField]
        /// radius of the circle that the preview tiles are scrolling on
        private float m_Radius = 20f;

        [SerializeField]
        /// center of the scroll circle
        private Transform m_CenterTransform;

        [SerializeField]
        /// arclength between to feed tiles
        private float m_DistanceBetweenFeeds = 5f;

        [SerializeField]
        /// Scroll speed in degrees per second
        private float m_RotationSpeed = 2f;

        [SerializeField]
        /// the view angle in degrees relative to center when scrolling speed is fastest
        private float m_MaxScrollViewAngle = 20f;

        [SerializeField]
        /// the horizontal view angle in degrees when user starts gazing and feed scrolling begins
        private float m_GazeStartAngle = 15f;

        [SerializeField]
        private Transform m_Camera;

        /// the offset angle between each preview tile in degrees
        private float m_OffsetAngle = 0f;

        private List<Transform> m_FeedPreviewTiles = new List<Transform>();

        /// the index of the last feed preview tile
        private int m_LastPositionIndex = 0;

        public bool m_EnableDebugMode = false;
        private bool m_IsUserScrolling = false;

        private int m_VioramasGrabbedFromDB = 0;

        /// Parent transform for active feed tiles
        private Transform m_ActiveFeedTilesParent;

        private Transform m_LastLeftElement;
        private Transform m_LastRightElement;

        private FeedManager m_FeedManager;

        private string m_SelectedCategory;

        private Transform m_FocusedFeedTile;

        #region DELEGATES_EVENTS
        public delegate void FeedTileChangedAction(Transform in_FeedTile);
        public event FeedTileChangedAction OnFeedTileFocused;

        public delegate void GazeChangedAction(bool in_IsGazing);
        public event GazeChangedAction OnGazeChanged;

        #endregion

        void Awake()
        {
            // create an object pool for all preview tiles
            m_FeedTilePrefab.CreatePool(m_InitialPoolSize);

            // change parenting
            ObjectPool.instance.transform.parent = transform;

            // create a game object for active tiles
            GameObject activeFeedTiles = new GameObject("Active Feed Tiles");
            m_ActiveFeedTilesParent = activeFeedTiles.transform;
            m_ActiveFeedTilesParent.parent = transform;


        }

        void OnEnable()
        {
            // if not assigned cache transform on this GO
            if (m_CenterTransform == null)
                m_CenterTransform = this.transform;
        }

        void Start()
        {

            // calculate offset in degrees between preview tiles based on the arclength between them
            m_OffsetAngle = (m_DistanceBetweenFeeds * 360) / (Mathf.PI * 2 * Radius);

            // get ref to feed manager
            m_FeedManager = FeedManager.Instance;
            m_SelectedCategory = m_FeedManager.GetFirstCategory();

            // retrieve a batch of vioramas from database
            List<ProtoViorama> l_Vioramas = m_FeedManager.FetchFeed(m_FeedManager.GetFirstCategory(), m_FeedPreviewTiles.Count, m_BatchSize);
            m_VioramasGrabbedFromDB += l_Vioramas.Count;

            foreach (ProtoViorama l_Viorama in l_Vioramas)
            {
                // spawn a new feed tile
                Transform feedTile = m_FeedTilePrefab.Spawn(m_ActiveFeedTilesParent);
                // assign a viorama to the tile
                VioramaFeedModel feedModel = feedTile.GetComponent<VioramaFeedModel>();
                feedModel.SwitchViorama(l_Viorama);

                m_FeedPreviewTiles.Add(feedTile);
            }

            DistributePreviews();

            // there is a new feed tile at the center -> fire event 
            if (OnFeedTileFocused != null)
                OnFeedTileFocused(m_FeedPreviewTiles[0]);
        }

        void Update()
        {
            // retrieve horizontal gaze angle
            Vector3 horizontalCameraDirection = m_Camera.forward;
            horizontalCameraDirection.y = 0f;

            float gazeAngle = Vector3.Angle(horizontalCameraDirection, CenterTransform.forward);

            if (gazeAngle > m_GazeStartAngle)
            {
                // set flag
                if (m_IsUserScrolling == false)
                {
                    m_IsUserScrolling = true;

                    
                    // fire gaze event
                    if (OnGazeChanged != null)
                        OnGazeChanged(true);
                }

                RotateTiles();

                SpawnOrDestroyTiles();
            }

            // snap preview tiles to grid if user is no longer scrolling
            if (m_IsUserScrolling == true && gazeAngle < m_GazeStartAngle)
            {
                m_IsUserScrolling = false;

                // fire gaze event
                if (OnGazeChanged != null)
                    OnGazeChanged(false);

                SnapPreviewTiles();
            }
        }

        private void SpawnOrDestroyTiles()
        {

            // spawn only if there are unspawned vioramas left in the database
            if(m_VioramasGrabbedFromDB < m_FeedManager.GetFeedCount(m_SelectedCategory))
            {
                // retrieve the angle of the last left element 
                Vector3 l_LastLeftDirection = m_LastLeftElement.position - CenterTransform.position;
                float l_LastLeftAngle = Vector3.Angle(l_LastLeftDirection, CenterTransform.forward);
                Vector3 l_CrossLeft = Vector3.Cross(CenterTransform.forward, l_LastLeftDirection);
                if (l_CrossLeft.y < 0) l_LastLeftAngle = -l_LastLeftAngle;

                // retrieve the angle of the last right element 
                Vector3 l_LastRightDirection = m_LastRightElement.position - CenterTransform.position;
                float l_LastRightAngle = Vector3.Angle(l_LastRightDirection, CenterTransform.forward);
                Vector3 l_CrossRight = Vector3.Cross(CenterTransform.forward, l_LastRightDirection);
                if (l_CrossRight.y < 0) l_LastRightAngle = -l_LastRightAngle;

                if (l_LastLeftAngle > (-m_SpawnNewFeedsAngle) && l_LastLeftAngle > -180)
                {
                    Debug.Log("Need to spawn elements left side");
                    Spawn(l_LastLeftAngle, SpawnMode.Left);

                }

                if (l_LastRightAngle < m_SpawnNewFeedsAngle && l_LastRightAngle < 180)
                {
                    Spawn(l_LastRightAngle, SpawnMode.Right);
                }

                m_LastLeftElement.GetComponent<VioramaFeedView>().m_AuthorLabel.text = l_LastLeftAngle.ToString();
                m_LastRightElement.GetComponent<VioramaFeedView>().m_AuthorLabel.text = l_LastRightAngle.ToString();
            }
        }

        private void RotateTiles()
        {
            // what is the gaze angle?
            float horizontalGazeAngle = Vector3.Angle(m_Camera.forward, CenterTransform.forward);
            float gazeMovePreviewTileFactor = map01(horizontalGazeAngle, 10f, m_MaxScrollViewAngle);

            Vector3 cross = Vector3.Cross(m_Camera.forward, CenterTransform.forward);
            if (cross.y < 0) gazeMovePreviewTileFactor = -gazeMovePreviewTileFactor;
            gazeMovePreviewTileFactor = Mathf.Clamp(gazeMovePreviewTileFactor, -1, 1);

            foreach (Transform previewTile in m_FeedPreviewTiles)
            {
                // get current angle of each preview tile
                Vector3 tileDirection = previewTile.position - CenterTransform.position;
                float angle = Vector3.Angle(tileDirection, CenterTransform.forward);

                cross = Vector3.Cross(CenterTransform.forward, tileDirection);
                if (cross.y < 0) angle = -angle;

                Vector3 desiredPosition = GetPositionOnCircle(angle + gazeMovePreviewTileFactor * m_RotationSpeed);
                previewTile.GetComponent<VioramaFeedView>().SetDesiredPosition(desiredPosition);
            }
        }

        private void SnapPreviewTiles()
        {
            foreach (Transform previewTile in m_FeedPreviewTiles)
            {
                // get current angle of each preview tile
                Vector3 tileDirection = previewTile.position - CenterTransform.position;

                // retrieve the current angle
                float angle = Vector3.Angle(tileDirection, CenterTransform.forward);

                Vector3 cross = Vector3.Cross(CenterTransform.forward, tileDirection);
                if (cross.y < 0) angle = -angle;
                // find closest angle on grid
                float gridAngle = Mathf.Round(angle / m_OffsetAngle) * m_OffsetAngle;

                if(gridAngle == 0f && m_FocusedFeedTile != previewTile)
                {
                    m_FocusedFeedTile = previewTile;

                    // fire event
                    if(OnFeedTileFocused != null)
                        OnFeedTileFocused(m_FocusedFeedTile);
                }
                Vector3 desiredPosition = GetPositionOnCircle(gridAngle);
                previewTile.GetComponent<VioramaFeedView>().SetDesiredPosition(desiredPosition);
            }
        }

        /// <summary>
        /// Distributes preview tiles evenly on a circle
        /// </summary>
        private void DistributePreviews()
        {
            // offset the start angle based on amounts of tiles
            float startAngle = 0;

            float previewObjectAngle = 0f;
            

            foreach(Transform feedTile in m_FeedPreviewTiles)
            {
                if(m_LastPositionIndex == 0 || m_LastPositionIndex > 0)
                {
                    previewObjectAngle = startAngle + m_OffsetAngle * m_LastPositionIndex;
                    ++m_LastPositionIndex;
                    m_LastPositionIndex *= -1;
                }
                else
                {
                    previewObjectAngle = startAngle + m_OffsetAngle * m_LastPositionIndex;
                    m_LastPositionIndex *= -1;
                }

                feedTile.position = GetPositionOnCircle(previewObjectAngle);

                Debug.Log("preview angle " + previewObjectAngle);
                VioramaFeedView feedView = feedTile.GetComponent<VioramaFeedView>();
                feedView.CenterPosition = CenterTransform.position;
                // look towards center position
                feedTile.rotation = Quaternion.LookRotation(feedTile.position - m_CenterTransform.position);
            }

            // update left / right end of feed
            if (m_LastPositionIndex < 0)
            {
                m_LastRightElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 1];
                m_LastLeftElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 2];
            }
            else
            {
                m_LastLeftElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 1];
                m_LastRightElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 2];
            }

            m_LastLeftElement.GetComponent<VioramaFeedView>().m_TitleLabel.text = "Left";
            m_LastRightElement.GetComponent<VioramaFeedView>().m_TitleLabel.text = "Right";
        }

        private void Spawn(float in_StartAngle, SpawnMode in_SpawnMode)
        {
            // retrieve a batch of vioramas from database
            List<ProtoViorama> l_Vioramas = m_FeedManager.FetchFeed(m_SelectedCategory, m_FeedPreviewTiles.Count, m_BatchSize);
            m_VioramasGrabbedFromDB += l_Vioramas.Count;

            for (int i=0; i<l_Vioramas.Count; ++i)
            {
                // spawn a new feed tile
                Transform feedTile = m_FeedTilePrefab.Spawn(m_ActiveFeedTilesParent);

                // calculate the spawn angle depending on whether spawning is done on left or right side
                float feedTileSpawnAngle = 0f;
                if (in_SpawnMode == SpawnMode.Left)
                { 
                    feedTileSpawnAngle = in_StartAngle - m_OffsetAngle * (i + 1);
                }

                if (in_SpawnMode == SpawnMode.Right)
                {
                    feedTileSpawnAngle = in_StartAngle + m_OffsetAngle * (i + 1);
                }

                feedTile.position = GetPositionOnCircle(feedTileSpawnAngle);
                // look towards center position
                feedTile.rotation = Quaternion.LookRotation(feedTile.position - m_CenterTransform.position);

                VioramaFeedView feedView = feedTile.GetComponent<VioramaFeedView>();
                feedView.CenterPosition = CenterTransform.position;

                // assign a viorama to the tile
                VioramaFeedModel feedModel = feedTile.GetComponent<VioramaFeedModel>();
                feedModel.SwitchViorama(l_Vioramas[i]);
                m_FeedPreviewTiles.Add(feedTile);
            }

            // update what element is at the end of the feed
            if (in_SpawnMode == SpawnMode.Left)
                m_LastLeftElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 1];
            if (in_SpawnMode == SpawnMode.Right)
                m_LastRightElement = m_FeedPreviewTiles[m_FeedPreviewTiles.Count - 1];
        }

        /// <summary>
        /// Finds the position on the circle based on an angle
        /// </summary>
        /// <param name="in_Angle"></param>
        /// <returns>The position on the circle</returns>
        private Vector3 GetPositionOnCircle(float in_Angle)
        {
            Vector3 position = new Vector3();
            position.x = CenterTransform.position.x + Radius * Mathf.Sin(Mathf.Deg2Rad * in_Angle);
            position.y = CenterTransform.position.y;
            position.z = CenterTransform.position.z + Radius * Mathf.Cos(Mathf.Deg2Rad * in_Angle);

            return position;
        }

        void OnDrawGizmos()
        {
            if (m_EnableDebugMode == true)
            {
                DebugExtension.DrawCircle(CenterTransform.position, Vector3.up, Color.green, Radius);

                Gizmos.color = Color.grey;

                foreach (Transform previewTrans in m_FeedPreviewTiles)
                {
                    Gizmos.DrawWireSphere(previewTrans.position, 0.5f);

                }

                // draw grid of valid preview tile positions
                int amountGridPoints = (int)(360 / m_OffsetAngle);
                float startAngle = 360 - amountGridPoints / 2 * m_OffsetAngle;

                for (int i = 0; i < amountGridPoints; ++i)
                {
                    Vector3 gridPointPosition = GetPositionOnCircle(startAngle + i * m_OffsetAngle);
                    DebugExtension.DrawPoint(gridPointPosition, Color.yellow, 1f);
                }
            }
        }

        // Maps a value from some arbitrary range to the 0 to 1 range
        public static float map01(float value, float min, float max)
        {
            return (value - min) * 1f / (max - min);
        }

        public float Radius
        {
            get
            {
                return m_Radius;
            }
        }

        public Transform CenterTransform
        {
            get
            {
                return m_CenterTransform;
            }
        }
    }
}
