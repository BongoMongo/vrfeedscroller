﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;

namespace Viorama
{
	/**
		Selects Feeds when user is gazing
	*/
	public class GazeSelector : MonoBehaviour 
	{
        #region MEMBER VARIABLES
        public Camera m_GazeCamera;
        public LayerMask m_SelectableMask;
        private bool m_IsUserGazing = false;

        private CardboardHead m_CardboardHead;

        private RaycastHit m_RaycastHit;
        private Vector3 m_SceenCenterPosition = new Vector3(0.5f, 0.5f, 0f);

        #endregion

        #region UNITY FUNCTIONS

        void OnEnable()
        {
            FeedScrollController scrollController = FeedScrollController.Instance;
            scrollController.OnGazeChanged += OnGazeChanged;
        }

        void OnDisable()
        {
            FeedScrollController scrollController = FeedScrollController.Instance;
            scrollController.OnGazeChanged -= OnGazeChanged;
        }

        public void Start()
        {
            StereoController stereoController = m_GazeCamera.GetComponent<StereoController>();
            m_CardboardHead = stereoController.Head;
        }


        void Update ()
		{
			if(m_IsUserGazing == true)
            {

                if (Physics.Raycast(m_CardboardHead.Gaze, out m_RaycastHit, m_SelectableMask))
                {
                    Transform objectHit = m_RaycastHit.transform;

                    Debug.Log("Selected " + objectHit.name);
                }
            }
		}

        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLER

        private void OnGazeChanged(bool in_IsGazing)
        {
            Debug.Log("Gaze state change to: " + in_IsGazing);
            m_IsUserGazing = in_IsGazing;
        }
        #endregion

        #region PROPERTIES
        #endregion

    }
}