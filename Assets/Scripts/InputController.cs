﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;

namespace Viorama
{
	/**
		This class does something for some reason.
	*/
	public class InputController : MonoBehaviour 
	{
		#region MEMBER VARIABLES
		#endregion
		
		#region UNITY FUNCTIONS

		void Awake ()
		{
			
		}
		
		void Start ()
		{
            // Disable screen dimming:
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            Cardboard.SDK.Recenter();
        }
		
		void Update ()
		{
  
		}
		
		#endregion
		
		#region FUNCTIONS
		#endregion
		
		#region EVENT HANDLER
		#endregion
		
		#region PROPERTIES
		#endregion
		
	}
}