﻿using UnityEngine;
using System.Collections;

public class MainController : Singleton<MainController> {

    private string m_CurrentSceneName;
    private string m_NextSceneName;

    private AsyncOperation m_ResourceUnloadTask;
    private AsyncOperation m_SceneLoadTask;
    
    private enum SceneState { Reset, Preload, Load, Unload, Postload, Ready, Run, Count };
    private SceneState m_CurrentState;
    private delegate void UpdateDelegate();
    private UpdateDelegate[] m_UpdateDelegates;

    public void SwitchScene(string in_NextSceneName)
    {
        if(m_CurrentSceneName != in_NextSceneName)
        {
            m_NextSceneName = in_NextSceneName;
        }
    }

    protected void Awake()
    {
        // keep the main controller alive between scene changes
        Object.DontDestroyOnLoad(gameObject);

        // setup array of update delegates
        m_UpdateDelegates = new UpdateDelegate[(int)SceneState.Count];
        m_UpdateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
        m_UpdateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
        m_UpdateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
        m_UpdateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
        m_UpdateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
        m_UpdateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
        m_UpdateDelegates[(int)SceneState.Run] = UpdateSceneRun;

        m_CurrentState = SceneState.Reset;
    }

    protected void Update()
    {
        // call method that corresponds to state
        if (m_UpdateDelegates[(int)m_CurrentState] != null)
            m_UpdateDelegates[(int)m_CurrentState]();
    }

    /// <summary>
    /// Resets scene
    /// </summary>
    private void UpdateSceneReset()
    {
        //  force a garbage collection
        System.GC.Collect();

        m_CurrentState = SceneState.Preload;
    }

    /// <summary>
    /// Handles anything that needs to happen before loading a new scene
    /// </summary>
    private void UpdateScenePreload()
    {
        m_SceneLoadTask = Application.LoadLevelAsync(m_NextSceneName);
        m_CurrentState = SceneState.Load;
    }

    /// <summary>
    /// Handles anything during scene loading
    /// </summary>
    private void UpdateSceneLoad()
    {
        // check if loading is finished
        if(m_SceneLoadTask.isDone == true)
        {
            m_CurrentState = SceneState.Unload;
        }
        else
        {
            Debug.Log("Loading scene: " + m_NextSceneName + ". Progess: " + m_SceneLoadTask.progress);
        }
    }

    /// <summary>
    /// Cleans up unused resources by unloading them
    /// </summary>
    private void UpdateSceneUnload()
    {
        // start new async operation for unloading resources
        if(m_ResourceUnloadTask == null)
        {
            m_ResourceUnloadTask = Resources.UnloadUnusedAssets();
        }
        // unloading not done yet
        else
        {
            if(m_ResourceUnloadTask.isDone == true)
            {
                // reset task
                m_ResourceUnloadTask = null;
                // loading & unloading is finished, so switch to postload state
                m_CurrentState = SceneState.Postload;
            }
            else
            {
                Debug.Log("Unloading resources. Progess: " + m_ResourceUnloadTask.progress);
            }
            
        }
    }

    /// <summary>
    /// Handles anything that need to happen immediately after loading a new scene
    /// </summary>
    private void UpdateScenePostload()
    {
        m_CurrentSceneName = m_NextSceneName;
        m_CurrentState = SceneState.Ready;
    }


    /// <summary>
    /// Handles anything that need to happen immediately before running the scene
    /// </summary>
    private void UpdateSceneReady()
    {
        // run another garbage collect
        System.GC.Collect();
        m_CurrentState = SceneState.Run;
    }

    /// <summary>
    /// Checks if a scene change is requested
    /// </summary>
    private void UpdateSceneRun()
    {
        if(m_CurrentSceneName != m_NextSceneName)
        {
            m_CurrentState = SceneState.Reset;
        }
    }

}
