﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;

namespace Viorama
{
	/**
		Changes the texture of the photosphere based on current selection
	*/
	public class Photosphere : MonoBehaviour 
	{
        #region MEMBER VARIABLES
        private VioramaFeedModel m_FocusedFeedModel;
        #endregion

        #region UNITY FUNCTIONS

        void OnEnable()
        {
            FeedScrollController scrollController = FeedScrollController.Instance;
            scrollController.OnFeedTileFocused += OnFeedTileFocused;
        }

        void OnDisable()
        {
            
            FeedScrollController scrollController = FeedScrollController.Instance;
            if(scrollController != null)
                scrollController.OnFeedTileFocused -= OnFeedTileFocused;
        }

        #endregion

        #region FUNCTIONS
        #endregion

        #region EVENT HANDLER
        private void OnFeedTileFocused(Transform in_FeedTile)
        {
            // remove connection to previous feed tile delegate
            if (m_FocusedFeedModel != null)
            {
                m_FocusedFeedModel.OnPhotosphereLoaded -= OnPhotosphereLoaded;
            }

            VioramaFeedModel feedModel = in_FeedTile.GetComponent<VioramaFeedModel>();

            // if there is already a photosphere texture available on the feed tile, use it
            if (feedModel.PhotosphereTexture != null)
            {
                GetComponent<Renderer>().material.mainTexture = feedModel.PhotosphereTexture;
            }
            // if not, setup handler and listen to it until the photosphere is fully loaded
            else
            {
                feedModel.OnPhotosphereLoaded += OnPhotosphereLoaded; ;
            }

            // store ref to the current, focused feed
            m_FocusedFeedModel = feedModel;
        }

        private void OnPhotosphereLoaded(Texture in_Texture)
        {
            GetComponent<Renderer>().material.mainTexture = in_Texture;
        }
        #endregion

        #region PROPERTIES
        #endregion

    }
}