﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;

namespace Viorama
{
	/**
		Testing class for FeedManager
	*/
	public class FeedManagerTester : MonoBehaviour 
	{
		#region MEMBER VARIABLES
		#endregion
		
		#region UNITY FUNCTIONS

		void Awake ()
		{
			
		}
		
		void Start ()
		{
           // FeedManager.Instance.FetchFeed("Desert");
           // FeedManager.Instance;
        }
		
		void Update ()
		{
			
		}
		
		#endregion
		
		#region FUNCTIONS
		#endregion
		
		#region EVENT HANDLER
		#endregion
		
		#region PROPERTIES
		#endregion
		
	}
}