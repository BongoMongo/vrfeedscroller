﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Viorama
{
	/**
		Shows information of the feed tile that is currently at the center position
	*/
	public class FeedCenterView : MonoBehaviour 
	{
        #region MEMBER VARIABLES
        public Text m_CategoryLabel;
        public Text m_TitleLabel;
        public Text m_AuthorLabel;
        public Slider m_LoadProgressBar;

        private VioramaFeedModel m_FocusedFeedModel;
        #endregion

        #region UNITY FUNCTIONS
        
        void OnEnable()
        {
            FeedScrollController scrollController = FeedScrollController.Instance;
            scrollController.OnFeedTileFocused += OnFeedTileFocused;
        }

        void OnDisable()
        {
            FeedScrollController scrollController = FeedScrollController.Instance;
            scrollController.OnFeedTileFocused -= OnFeedTileFocused;
        }

        void Start()
        {
            // hide all label
            m_CategoryLabel.enabled = false;
            m_TitleLabel.enabled = false;
            m_AuthorLabel.enabled = false;

            // hide progress bar
            if (m_LoadProgressBar != null)
            {
                m_LoadProgressBar.gameObject.SetActive(false);
            }
        }

    #endregion

    #region FUNCTIONS
    #endregion

    #region EVENT HANDLER
    private void OnFeedTileFocused(Transform in_FeedTile)
        {
            // remove connection to previous feed tile delegate
            if(m_FocusedFeedModel != null)
            {
                m_FocusedFeedModel.OnCategoryChanged -= OnCategoryChanged;
                m_FocusedFeedModel.OnTitleChanged -= OnTitleChanged;
                m_FocusedFeedModel.OnAuthorChanged -= OnAuthorChanged;
                m_FocusedFeedModel.OnPhotosphereLoaded -= OnPhotosphereLoaded;
                m_FocusedFeedModel.OnLoadProgressChanged -= OnLoadProgressChanged;
            }

            VioramaFeedModel feedModel = in_FeedTile.GetComponent<VioramaFeedModel>();

            // if there is already a viorama available on the feed tile, use its information
            if(feedModel.LoadedViorama != null)
            {
                m_CategoryLabel.text = EnumUtils.stringValueOf(feedModel.LoadedViorama.Category);
                m_AuthorLabel.text = feedModel.LoadedViorama.Author;
                m_TitleLabel.text = feedModel.LoadedViorama.Title;

                // show labels
                if (m_CategoryLabel.enabled == false)
                    m_CategoryLabel.enabled = true;
                if (m_TitleLabel.enabled == false)
                    m_TitleLabel.enabled = true;
                if (m_AuthorLabel.enabled == false)
                    m_AuthorLabel.enabled = true;

            }
            // if not, setup handler and listen to it until everything is ready
            else
            {
                feedModel.OnCategoryChanged += OnCategoryChanged;
                feedModel.OnTitleChanged += OnTitleChanged;
                feedModel.OnAuthorChanged += OnAuthorChanged;
                feedModel.OnPhotosphereLoaded += OnPhotosphereLoaded;
                feedModel.OnLoadProgressChanged += OnLoadProgressChanged;
            }

            // store ref to the current, focused feed
            m_FocusedFeedModel = feedModel;
        }

        private void OnCategoryChanged(string in_Category)
        {
            if (m_CategoryLabel != null)
            {
                // show label
                if (m_CategoryLabel.enabled == false)
                    m_CategoryLabel.enabled = true;

                m_CategoryLabel.text = in_Category;
            }

        }

        private void OnTitleChanged(string in_Title)
        {
            if (m_TitleLabel != null)
            {
                if (m_TitleLabel.enabled == false)
                    m_TitleLabel.enabled = true;
                m_TitleLabel.text = in_Title;
            }
               
        }

        private void OnAuthorChanged(string in_Author)
        {
            if (m_AuthorLabel != null)
            {
                if (m_AuthorLabel.enabled == false)
                    m_AuthorLabel.enabled = true;
                m_AuthorLabel.text = in_Author;
            }
        }

        private void OnLoadProgressChanged(float in_Value)
        {
            if (m_LoadProgressBar != null)
            {
                // show progress bar if hidden
                if (m_LoadProgressBar.gameObject.activeSelf == false)
                {
                    m_LoadProgressBar.gameObject.SetActive(true);
                }

                m_LoadProgressBar.value = in_Value;
            }
        }

        private void OnPhotosphereLoaded(Texture in_Texture)
        {
            // hide progress bar if photosphere has finished loading
            if (m_LoadProgressBar != null && m_LoadProgressBar.gameObject.activeSelf == true)
            {
                m_LoadProgressBar.gameObject.SetActive(false);
            }
        }

        #endregion

        #region PROPERTIES
        #endregion

    }
}