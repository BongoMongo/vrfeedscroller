﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/
using UnityEngine;
using System.Collections;

namespace Viorama
{
	/**
		Loads and manages the data for a single feed object
	*/
	public class VioramaFeedModel : MonoBehaviour 
	{
        #region MEMBER VARIABLES

        public Texture m_PreviewTexture;
        public Texture m_PhotosphereTexture;

        private ProtoViorama m_CurrentViorama;
        private ProtoViorama m_NextViorama;

        private enum FeedState { Reset, PreloadPreview, LoadPreview, PostLoadPreview, Load, Postload, Ready, Run, Count };
        private FeedState m_CurrentState;

        private delegate void UpdateDelegate();
        private UpdateDelegate[] m_UpdateDelegates;

        private WWW m_PreviewWWW;
        private WWW m_PhotosphereWWW;

        #endregion

        #region DELEGATES_EVENTS
        public delegate void ImageLoadAction(Texture in_Texture);
        public event ImageLoadAction OnPreviewLoaded;
        public event ImageLoadAction OnPhotosphereLoaded;

        public delegate void LoadAction(float in_Value);
        public event LoadAction OnLoadProgressChanged;

        public delegate void StringLoadAction(string in_Value);
        public event StringLoadAction OnCategoryChanged;
        public event StringLoadAction OnTitleChanged;
        public event StringLoadAction OnAuthorChanged;
        #endregion


        #region UNITY FUNCTIONS

        void Awake ()
		{
            // setup array of update delegates
            m_UpdateDelegates = new UpdateDelegate[(int)FeedState.Count];
            m_UpdateDelegates[(int)FeedState.Reset] = UpdateFeedReset;
            m_UpdateDelegates[(int)FeedState.PreloadPreview] = UpdateFeedPreviewPreload;
            m_UpdateDelegates[(int)FeedState.LoadPreview] = UpdateFeedPreviewLoad;
            m_UpdateDelegates[(int)FeedState.PostLoadPreview] = UpdatePostLoadPreview;
            m_UpdateDelegates[(int)FeedState.Load] = UpdateLoad;
            m_UpdateDelegates[(int)FeedState.Postload] = UpdateFeedPostload;
            m_UpdateDelegates[(int)FeedState.Ready] = UpdateFeedReady;
            m_UpdateDelegates[(int)FeedState.Run] = UpdateFeedRun;

            m_CurrentState = FeedState.Run;
        }

        protected void Update()
        {
            // call method that corresponds to state
            if (m_UpdateDelegates[(int)m_CurrentState] != null)
                m_UpdateDelegates[(int)m_CurrentState]();
        }

        #endregion

        #region FUNCTIONS

        /// <summary>
        /// Switches and load a diffent Viorama
        /// </summary>
        /// <param name="in_Viorama">Viorama to switch to</param>
        public void SwitchViorama(ProtoViorama in_Viorama)
        {
            if (m_CurrentViorama != m_NextViorama || m_CurrentViorama == null)
            {
                m_NextViorama = in_Viorama;
            }
        }

        /// <summary>
        /// Resets feed
        /// </summary>
        private void UpdateFeedReset()
        {
            //  force a garbage collection
            System.GC.Collect();

            m_CurrentState = FeedState.PreloadPreview;
        }

        /// <summary>
        /// Starts loading the feed preview & the photosphere images
        /// </summary>
        private void UpdateFeedPreviewPreload()
        {
            // start loading preview image
            m_PreviewWWW = new WWW(m_NextViorama.PreviewURL);
            // start loading the photosphere
            m_PhotosphereWWW = new WWW(m_NextViorama.ImageURL);

            // Feed data has changed -> fire events

            // 1. Category
            if (OnCategoryChanged != null)
                OnCategoryChanged(EnumUtils.stringValueOf(m_NextViorama.Category));

            // 3. Title
            if (OnTitleChanged != null)
                OnTitleChanged(m_NextViorama.Title);

            // 4. Author
            if (OnAuthorChanged != null)
                OnAuthorChanged(m_NextViorama.Author);

            m_CurrentState = FeedState.LoadPreview;
        }

        /// <summary>
        /// Handles anything during preview image loading
        /// </summary>
        private void UpdateFeedPreviewLoad()
        {
            // check if loading is finished
            if (m_PreviewWWW.isDone == true)
            {
                m_CurrentState = FeedState.PostLoadPreview;
            }
        }

        /// <summary>
        /// Handles everything after loading of preview images has finished
        /// </summary>
        private void UpdatePostLoadPreview()
        {
            if(m_PreviewWWW != null)
            {
                m_PreviewTexture = m_PreviewWWW.texture;

                // fire event
                if (OnPreviewLoaded != null)
                    OnPreviewLoaded(m_PreviewTexture);
            }
           
            // reset WWW module
            m_PreviewWWW = null;

            m_CurrentState = FeedState.Load;
        }

        /// <summary>
        /// Handles anything during photosphere image loading
        /// </summary>
        private void UpdateLoad()
        {
            // check if loading is finished
            if (m_PhotosphereWWW.isDone == true)
            {
                m_CurrentState = FeedState.Postload;
            }
            else
            {
                // fire event
                if (OnLoadProgressChanged != null)
                    OnLoadProgressChanged(m_PhotosphereWWW.progress);

               // Debug.Log("Loading feed photosphere: " + m_NextViorama.Title + ". Progess: " + m_PhotosphereWWW.progress);
            }
        }

        /// <summary>
        /// Handles anything that need to happen immediately after loading a new feed
        /// </summary>
        private void UpdateFeedPostload()
        {
            m_PhotosphereTexture = m_PhotosphereWWW.texture;

            // fire event
            if (OnPhotosphereLoaded != null)
                OnPhotosphereLoaded(m_PhotosphereTexture);

            // reset WWW module
            m_PhotosphereWWW = null;

            // done with everything; viorama can be assigned now
            m_CurrentViorama = m_NextViorama;

            m_CurrentState = FeedState.Ready;
        }

        /// <summary>
        /// Handles anything that need to happen immediately before running the scene
        /// </summary>
        private void UpdateFeedReady()
        {
            // run another garbage collect (old viorama now gets released from memory)
            System.GC.Collect();
            m_CurrentState = FeedState.Run;
        }

        /// <summary>
        /// Checks if a scene change is requested
        /// </summary>
        private void UpdateFeedRun()
        {
            if (m_CurrentViorama != m_NextViorama)
            {
                m_CurrentState = FeedState.Reset;
            }
        }
        #endregion

        #region EVENT HANDLER
        #endregion

        #region PROPERTIES
        public ProtoViorama LoadedViorama
        {
            get
            {
                return m_CurrentViorama;
            }
        }

        public Texture PhotosphereTexture
        {
            get
            {
                return m_PhotosphereTexture;
            }
        }
        #endregion

    }
}