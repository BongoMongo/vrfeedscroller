﻿/*
------------------------------------------------------------------
Copyright (c) 2015 VIORAMA UG  
This software is the proprietary information of VIORAMA UG
All rights reserved.
------------------------------------------------------------------
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Viorama
{
    /**
		Displays a Viorama Feed
	*/
    public class VioramaFeedView : MonoBehaviour
    {
        #region MEMBER VARIABLES
        public Canvas m_UI;
        public Text m_CategoryLabel;
        public Text m_TitleLabel;
        public Text m_AuthorLabel;
        public Slider m_LoadProgressBar;

        public Renderer m_Renderer;

        private Vector3 m_DesiredPosition;

        private float m_StopThreshold = 0.1f;

        private Quaternion m_CurrentOrientation;

        private Vector3 m_CenterPosition;

        #endregion

        #region UNITY FUNCTIONS
        void Start()
        {
            m_DesiredPosition = transform.position;

            // hide progress bar
            if (m_LoadProgressBar != null)
            {
                m_LoadProgressBar.gameObject.SetActive(false);
            }
        }

        void OnEnable()
        {
            // add handlers
            VioramaFeedModel feedModel = GetComponent<VioramaFeedModel>();
            feedModel.OnPreviewLoaded += OnPreviewLoaded;
            feedModel.OnPhotosphereLoaded += OnPhotosphereLoaded;
            feedModel.OnLoadProgressChanged += OnLoadProgressChanged;
            feedModel.OnCategoryChanged += OnCategoryChanged;
            feedModel.OnAuthorChanged += OnAuthorChanged;
            feedModel.OnTitleChanged += OnTitleChanged;
        }

        void OnDisable()
        {
            // remove handlers
            VioramaFeedModel feedModel = GetComponent<VioramaFeedModel>();
            feedModel.OnPreviewLoaded -= OnPreviewLoaded;
            feedModel.OnPhotosphereLoaded -= OnPhotosphereLoaded;
            feedModel.OnLoadProgressChanged -= OnLoadProgressChanged;
            feedModel.OnCategoryChanged -= OnCategoryChanged;
            feedModel.OnAuthorChanged -= OnAuthorChanged;
            feedModel.OnTitleChanged -= OnTitleChanged;
        }

        void Update()
        {
            if (Vector3.Distance(m_DesiredPosition, transform.position) > m_StopThreshold)
            {
                LerpTowardsPostion();
            }
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(m_DesiredPosition, 0.2f);

        }
        #endregion

        #region FUNCTIONS

        public void SetUIActive(bool in_IsActive)
        {
            m_UI.enabled = in_IsActive;
        }

        public void SetDesiredPosition(Vector3 in_Position)
        {
            this.m_DesiredPosition = in_Position;

        }

        private void LerpTowardsPostion()
        {

            Vector3 currPosRelCenter = transform.position - m_CenterPosition;
            Vector3 desiredPosRelCenter = m_DesiredPosition - m_CenterPosition;

            transform.position = Vector3.Slerp(currPosRelCenter, desiredPosRelCenter, Time.deltaTime * 5f);
            transform.position += m_CenterPosition;

            // look towards center position
            transform.rotation = Quaternion.LookRotation(transform.position - m_CenterPosition);
        }

        public Vector3 CenterPosition
        {
            get
            {
                return m_CenterPosition;
            }

            set
            {
                m_CenterPosition = value;
            }
        }



        #endregion

        #region EVENT HANDLER
        private void OnTitleChanged(string in_Title)
        {
            if (m_TitleLabel != null)
                m_TitleLabel.text = in_Title;
        }

        private void OnAuthorChanged(string in_Author)
        {
            if (m_AuthorLabel != null)
                m_AuthorLabel.text = in_Author;
        }


        private void OnLoadProgressChanged(float in_Value)
        {
           if(m_LoadProgressBar != null)
            {
                // show progress bar if hidden
                if(m_LoadProgressBar.gameObject.activeSelf == false)
                {
                    m_LoadProgressBar.gameObject.SetActive(true);
                }

                m_LoadProgressBar.value = in_Value;
            }
        }

        private void OnPhotosphereLoaded(Texture in_Texture)
        {
            // hide progress bar if photosphere has finished loading
            if (m_LoadProgressBar != null && m_LoadProgressBar.gameObject.activeSelf == true)
            {
                m_LoadProgressBar.gameObject.SetActive(false);
            }
        }

        private void OnPreviewLoaded(Texture in_Texture)
        {
            if (m_Renderer != null)
                m_Renderer.material.mainTexture = in_Texture;
        }

        private void OnCategoryChanged(string in_Category)
        {
            if (m_CategoryLabel != null)
                m_CategoryLabel.text = in_Category;
        }

        #endregion
    }
}
